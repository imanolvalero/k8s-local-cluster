#!/bin/bash
#=========================================================================
# Deployment limits
LIMIT_MIN_MASTER_QUANTITY=1
LIMIT_MAX_MASTER_QUANTITY=4
LIMIT_MIN_MASTER_CPUS=2
LIMIT_MIN_MASTER_RAM=2048
LIMIT_MIN_WORKER_QUANTITY=1
LIMIT_MAX_WORKER_QUANTITY=9
LIMIT_MIN_WORKER_CPUS=1
LIMIT_MIN_WORKER_RAM=1024
WARN_CPU_USAGE=60
WARN_RAM_USAGE=80

#=========================================================================
# Dynamic config files
VAGRANT_FILE=Vagrantfile
INVENTORY_FILE=.inventory
HOSTS_FILE=.hosts
DEPLOY_FILE=.deploy.yaml

#=========================================================================
# Read config from environment
PROJECT=${K8SLC_PROJECT:-$(echo ${PWD##*/} | sed 's/^\.//g')}
MASTER_QUANTITY=${K8SLC_MASTER_QUANTITY:-"1"}
MASTER_CPUS=${K8SLC_MASTER_CPUS:-"2"}
MASTER_RAM=${K8SLC_MASTER_RAM:-"2048"}
WORKER_QUANTITY=${K8SLC_WORKER_QUANTITY:-"2"}
WORKER_CPUS=${K8SLC_WORKER_CPUS:-"1"}
WORKER_RAM=${K8SLC_WORKER_RAM:-"1024"}
K8S_FLAVOUR=${K8SLC_K8S_FLAVOUR:-"xenial"}
K8S_VERSION=${K8SLC_K8S_VERSION:-"1.22.3-00"}
BOX=${K8SLC_BOX:-"generic/ubuntu1804"}
NODE_NETWORK=${K8SLC_NODE_NETWORK:-"172.16.100.0"}
POD_NETWORK=${K8SLC_POD_NETWORK:-"172.16.101.0"}


#=========================================================================
# ShowHelp function
function showhelp(){
    cat << EOF
Local HA Kubernetes cluster deployment tool
Usage:
${0} [options]
Options:
-p,  --project       project name.
                     will override value of env K8SLC_PROJECT
                     default: <current-directory-name>
-m,  --masters       controller node quantity.
                     will override value of env K8SLC_MASTER_QUANTITY
                     default: 1; min: 1; max: 4
-mc, --master-cpus   controller node CPU quantity.
                     will override value of env K8SLC_MASTER_CPUS
                     default: 2; min: 2
-mr, --master-ram    controller node RAM ammount.
                     will override value of env K8SLC_MASTER_RAM
                     default: 2048; min: 2048
-w,  --workers       worker node quantity.
                     will override value of env K8SLC_WORKER_QUANTITY
                     default: 2; min: 1; max: 9
-wc, --worker-cpus   worker node CPU quantity.
                     will override value of env K8SLC_WORKER_CPUS
                     default: 1; min: 1
-wr, --worker-ram    worker node RAM ammount.
                     will override value of env K8SLC_WORKER_RAM
                     default: 1024; min: 1024
-kf, --k8s-flavour   flavour kubernetes-* to install from 
                     https://packages.cloud.google.com/apt/dists.
                     will override value of env K8SLC_K8S_FLAVOUR
                     default: xenial
-kv, --k8s-version   version of packages to install
                     will override value of env K8SLC_K8S_VERSION
                     default: 1.22.3-00
-b,  --box           box of Vagrant to use.
                     will override value of env K8SLC_BOX
                     default: generic/ubuntu1804
-nn, --node-network  IP/24 of the nodes subnet. 
                     will override value of env K8SLC_NODE_NETWORK
                     default: 172.16.100.0
-pn, --pod-network   IP/24 of the nodes subnet. 
                     will override value of env K8SLC_POD_NETWORK
                     default: 172.16.101.0
-h,  --help          prints this help message.

More info in:
https://gitlab.com/imanolvalero/k8s-local-cluster

EOF
    exit
}

#=========================================================================
# Read config from script arguments
while [ $# -ne 0 ]; do
    case "$1" in
    -p|--project)
        PROJECT="$2"
        shift
        ;;
    -m|--masters)
        MASTER_QUANTITY="$2"
        shift
        ;;
    -mc|--master-cpus)
        MASTER_CPUS="$2"
        shift
        ;;
    -mr|--master-ram)
        MASTER_RAM="$2"
        shift
        ;;
    -w|--workers)
        WORKER_QUANTITY="$2"
        shift
        ;;
    -wc|--worker-cpus)
        WORKER_CPUS="$2"
        shift
        ;;
    -wr|--worker-ram)
        WORKER_RAM="$2"
        shift
        ;;
    -kf|--k8s-flavour)
        MASTER_RAM="$2"
        shift
        ;;
    -kv|--k8s-version)
        WORKER_CPUS="$2"
        shift
        ;;
    -b|--box)
        BOX="$2"
        shift
        ;;
    -nn|--node-network)
        NODE_NETWORK="$2"
        shift
        ;;
    -pn|--pod-network)
        POD_NETWORK="$2"
        shift
        ;;
    -h|--help)
        showhelp
        ;;
    *)
        echo "Argument incorrect: $1"
        showhelp
        ;;
    esac
    shift
done

#=========================================================================
# Clean and prepare inputs
# Obtain IP prefix
IFS=. IP_RANGES=(${NODE_NETWORK}) IFS=$' \t\n'
IP_PREFIX=${IP_RANGES[0]}.${IP_RANGES[1]}.${IP_RANGES[2]}

# Obtain Kuberbetes version for kubelet config file
KUBELET_VERSION=$(echo ${K8S_VERSION} | sed 's/\(.*\)\-.*/\1/')

# Sanity project name
PROJECT="${PROJECT// /_}"

# retrive script name
SCRIPT_NAME=$(basename ${0} | sed 's/\(.*\)\..*/\1/')

#=========================================================================
# Confirmation
function confimation_line() {
    printf "${PROJECT}-${1}${2}\t${BOX}\t${IP_PREFIX}.${3}\t${4}MB RAM ${5}vCPU\n" 
}
printf "\nThis script will create the following infrastructure:\n\n"

declare -a array
array[0]="${MASTER_QUANTITY} 10 master ${MASTER_RAM} ${MASTER_CPUS}"
array[1]="${WORKER_QUANTITY} 20 worker ${WORKER_RAM} ${WORKER_CPUS}"
for element in "${array[@]}"; do
    IFS=" " read -r -a values <<< "${element}"
    for ((i=1; i<=${values[0]}; i++)); do
        confimation_line ${values[2]} ${i} $(( ${values[1]} + ${i} )) \
            ${values[3]} ${values[4]}
    done
done

printf "\nThis action will remove vm's and ip's keys from known_hosts\n\n"
read -p "Do you want to continue [Y|n]? " TMP
TMP=${TMP::1}
[[ -n "${TMP}" && "${TMP,,}" != "y" ]] && exit

#=========================================================================
# Check limits and resources requirements
#
AVAILABLE_CPUS=$(cat /proc/cpuinfo | grep cores | awk '{ n += $4}; END { print n }')
AVAILABLE_RAM=$(free -m | grep "Mem:" | awk '{ print $4}')
ESTIMATED_CPUS=$(( ${MASTER_CPUS} * ${MASTER_QUANTITY} + ${WORKER_CPUS} * ${WORKER_QUANTITY} ))
ESTIMATED_RAM=$(( ${MASTER_RAM} * ${MASTER_QUANTITY} + ${WORKER_RAM} * ${WORKER_QUANTITY} ))

MESSAGE=""
[[ ${MASTER_QUANTITY} -lt ${LIMIT_MIN_MASTER_QUANTITY} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum quantity for masters is ${LIMIT_MIN_MASTER_QUANTITY}\e[0m\n"

[[ ${MASTER_QUANTITY} -gt ${LIMIT_MAX_MASTER_QUANTITY} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMaximum quantity for masters is ${LIMIT_MAX_MASTER_QUANTITY}\e[0m\n"

[[ ${MASTER_CPUS} -lt ${LIMIT_MIN_MASTER_CPUS} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum CPU for masters is ${LIMIT_MIN_MASTER_CPUS}\e[0m\n"

[[ ${MASTER_RAM} -lt ${LIMIT_MIN_MASTER_RAM} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum RAM for masters is ${LIMIT_MIN_MASTER_RAM}\e[0m\n"

[[ ${WORKER_QUANTITY} -lt ${LIMIT_MIN_WORKER_QUANTITY} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum quantity for workers is ${LIMIT_MIN_WORKER_QUANTITY}\e[0m\n"

[[ ${WORKER_QUANTITY} -gt ${LIMIT_MAX_WORKER_QUANTITY} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMaximum quantity for workers is ${LIMIT_MAX_WORKER_QUANTITY}\e[0m\n"

[[ ${WORKER_CPUS} -lt ${LIMIT_MIN_WORKER_CPUS} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum CPU for workers is ${LIMIT_MIN_WORKER_CPUS}\e[0m\n"

[[ ${WORKER_RAM} -lt ${LIMIT_MIN_WORKER_RAM} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mMinimum RAM for masters is ${LIMIT_MIN_WORKER_RAM}\e[0m\n"

[[ ${AVAILABLE_RAM} -lt ${ESTIMATED_RAM} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mThis cluster would consume more RAM than available\e[0m\n"

[[ ${AVAILABLE_CPUS} -lt ${ESTIMATED_CPUS} ]] && \
    MESSAGE="${MESSAGE}\e[1;31mThis cluster would consume more CPU than available\e[0m\n"

[[ -n "${MESSAGE}" ]] && printf "\n${MESSAGE}\nAborting cluster creation\n\n" && exit
    
ESTIMATED_CPUS=$(( ${ESTIMATED_CPUS} * 100 / ${AVAILABLE_CPUS} ))
[[ ${ESTIMATED_CPUS} -gt ${WARN_CPU_USAGE} ]] && \
    MESSAGE="${MESSAGE}\e[1;33mThis cluster will consume ${ESTIMATED_CPUS}%% of available CPU\e[0m\n"

ESTIMATED_RAM=$(( ${ESTIMATED_RAM} * 100 / ${AVAILABLE_RAM} ))
[[ ${ESTIMATED_RAM} -gt ${WARN_RAM_USAGE} ]] && \
    MESSAGE="${MESSAGE}\e[1;33mThis cluster will consume ${ESTIMATED_RAM}%% of available RAM\e[0m\n"

if [[ -n "${MESSAGE}" ]]; then
    TMP=""
    MESSAGE="${MESSAGE}\n\e[1;33mGoing ahead could end up with your computer in unstable state\e[0m"
    printf "\n${MESSAGE}\n\nAre you sure do you want to continue?\n"
    read -p "Write the word 'yes' to continue or any other thing to abort cluster creation: " TMP

    [[ "${TMP}" != "yes" ]] && \
        printf "\n\e[1;32mGood choice\e[0m\nAborting cluster creation\n\n" && \
        exit

    printf '\n\e[1mOK. It is your decission.\e[0m\n'
    TMP=9
    while [ ${TMP} -gt -1 ]; do
        printf "\rGoing ahead with the cluster creation in ${TMP}"
        TMP=$(( ${TMP} - 1 ))
        sleep 1
    done
    printf "\r\e[1;32mGoing ahead with the cluster creation!      \e[0m\n\n"
fi

#=========================================================================
# Vagrantfile
[[ -f ${VAGRANT_FILE} ]] && rm ${VAGRANT_FILE}
cat <<EOF >> ${VAGRANT_FILE}
Vagrant.configure("2") do |config|
    config.vm.provision "shell", inline: <<-'SCRIPT'
    apt-get update -y
    apt-get upgrade -y
    apt-get autoremove -y
    apt-get autoclean -y
    SCRIPT

    (1..${MASTER_QUANTITY}).each do |machine_id|
        config.vm.define "${PROJECT}-master#{machine_id}" do |controler|
            controler.vm.box = "${BOX}"
            controler.vm.hostname = "${PROJECT}-master#{machine_id}"
            controler.vm.network "private_network", ip: "${IP_PREFIX}.#{10+machine_id}"
            controler.vm.provider "libvirt" do |controlervm|
                controlervm.cpus = ${MASTER_CPUS}
                controlervm.memory = ${MASTER_RAM}
            end
        end
    end
    
    (1..${WORKER_QUANTITY}).each do |machine_id|
        config.vm.define "${PROJECT}-worker#{machine_id}" do |node|
            node.vm.box = "${BOX}"
            node.vm.hostname = "${PROJECT}-worker#{machine_id}"
            node.vm.network "private_network", ip: "${IP_PREFIX}.#{20+machine_id}"
            node.vm.provider "libvirt" do |nodevm|
                nodevm.cpus = ${WORKER_CPUS}
                nodevm.memory = ${WORKER_RAM}
            end
        end
    end
end
EOF

#=========================================================================
# Create temporary and config files
# .inventory  .hosts  .deploy.yaml
[[ -f ${INVENTORY_FILE} ]] && rm ${INVENTORY_FILE}
[[ -f ${HOSTS_FILE} ]] && rm ${HOSTS_FILE}
[[ -f ${DEPLOY_FILE} ]] && rm ${DEPLOY_FILE}

function inventory_line(){
    echo "${2} ansible_host=${1} ansible_ssh_common_args='-i .vagrant/machines/${2}/libvirt/private_key'"
}
function hosts_line(){
    echo "${1} ${2}"
}
function server_line(){
    echo "                server ${2} ${1}:6443 check"
}

# File headers
## .deploy.yaml
cat << EOF > ${DEPLOY_FILE}
_params:
  k8s:
    package_version: ${K8S_VERSION}
    flavour: ${K8S_FLAVOUR}
    version: ${KUBELET_VERSION}
  apiserver:
    vip: ${IP_PREFIX}.31
  networks:
    nodes: ${NODE_NETWORK}
    pods: ${POD_NETWORK}
  project:
    name: ${PROJECT}
  control_pane_endpoint: ${PROJECT}-master-vip
  haproxy:
    master: ${PROJECT}-master1
    servers: |
EOF

# File bodies
declare -a array
array[0]="controllers ${MASTER_QUANTITY} 10 master"
array[1]="workers ${WORKER_QUANTITY} 20 worker"

for element in "${array[@]}"; do
    IFS=" " read -r -a values <<< "${element}"

    for ((i=1; i<=${values[1]}; i++));  do
        VM_IP=${IP_PREFIX}.$(( ${values[2]} + ${i} ))
        VM_NAME=${PROJECT}-${values[3]}${i}

        ## .deploy.yaml
        if [[ "${values[0]}" == "controllers" ]]; then
            server_line ${VM_IP} ${VM_NAME}  >> ${DEPLOY_FILE}
        fi

        ## .inventory.ini
        if [[ ${i} -gt 1 ]]; then 
            inventory_line ${VM_IP} ${VM_NAME}  >> ${INVENTORY_FILE}
        else
            if [[ "${values[0]}" == "controllers" ]]; then 
                cat << EOF >> ${INVENTORY_FILE}
[master_controller]        
$(inventory_line ${VM_IP} ${VM_NAME})

[slave_controllers]
EOF
            else 
                cat << EOF >> ${INVENTORY_FILE}

[controllers:children]
master_controller
slave_controllers

[workers]
$(inventory_line ${VM_IP} ${VM_NAME})
EOF
            fi
        fi
       
        ## .hosts
        hosts_line ${VM_IP} ${VM_NAME} >> ${HOSTS_FILE}
    done
done

# File footers
## .inventory.ini
cat <<EOF >> ${INVENTORY_FILE}

[all:vars]
ansible_ssh_user='vagrant'
ansible_ssh_common_args='-p 22 -o LogLevel=FATAL -o Compression=yes -o DSAAuthentication=yes -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
EOF
## .hosts
hosts_line ${IP_PREFIX}.31 ${PROJECT}-master-vip >> ${HOSTS_FILE}

#=========================================================================
# Clean previous keys from known_hosts
declare -a array
array[0]="${MASTER_QUANTITY} 10 master"
array[1]="${WORKER_QUANTITY} 20 worker"
for element in "${array[@]}"; do
    IFS=" " read -r -a values <<< "${element}"
    for ((i=1; i<=${values[0]}; i++)); do
        ssh-keygen -R "${IP_PREFIX}.$(( ${values[1]} + ${i} ))" &>/dev/null
        ssh-keygen -R "${PROJECT}-${values[2]}${i}" &>/dev/null
    done
done

#=========================================================================
# Create vms
export ANSIBLE_HOST_KEY_CHECKING=False
vagrant destroy --force
vagrant up

#=========================================================================
# Deploy k8s
ansible-playbook -i ${INVENTORY_FILE} ${SCRIPT_NAME}.yaml
TMP=$?

#=========================================================================
# /etc/hosts advise
if [[ ${TMP} -eq 0 ]]; then
    MESSAGE="It is done!\n You can access to virtual machines using the command\n"
    MESSAGE="${MESSAGE}\tvagrant ssh <vm-name>\n"

    if [[ ${MASTER_QUANTITY} -gt 1 ]]; then
        MESSAGE="${MESSAGE}\nAs this is a load-balanced cluster, for accessing to cluster from host, "
        MESSAGE="${MESSAGE}you should add the load balancer IP data in your /etc/hosts file.\n"
        MESSAGE="${MESSAGE}you can doit executing the following command:\n\n"
        MESSAGE="${MESSAGE}\t echo ${IP_PREFIX}.31  ${PROJECT}-master-vip | sudo tee -a /etc/hosts\n"
    else
        sed -i 's/${PROJECT}-master-vip/${IP_PREFIX}.31/g' .kubeconfig
    fi
    printf "${MESSAGE}\n"
fi

