# Local highly available Kubernetes cluster

### Description
This script creatres an Highly Available `Kubernetes` cluster using `containerd`, over `Linux` and `libvirt`

This script automates the steps detailed [k8s-fundamentals of Lemoncode](https://github.com/Lemoncode/k8s-fundamentals/tree/main/00-installing-k8s/02-kubeadm-local-containerd).

For **HA topology**, I have follow steps detailed in [Kubernetes official docs](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/ha-topology/)
![stacked etc](https://d33wubrfki0l68.cloudfront.net/d1411cded83856552f37911eb4522d9887ca4e83/b94b2/images/kubeadm/kubeadm-ha-topology-stacked-etcd.svg)

**Load-balancing** capacities has been added thanks to add [MetalLB](https://metallb.org/)

**Dynamic storage provisioning** support has been added thanks to a [nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner). An automatic provisiones that uses the new **NFS server**, added by default in the main control-plane.


**Important security considerations have not been taken in account.**
**Please, do not use in production.**

### Motivation
I created for these reasons:
* On my [kubernetes learning path](https://gitlab.com/imanolvalero/lemoncode-devops/-/tree/main/modulo3-orquestacion/k8s-local-cluster), I need a tool that automates the deployment of a cluster on my desktop environment (`Linux` + `libvirt`).
  Now, I am able to get a new cluster up for learning, in a couple of minutes.
* Practice `Bash scripting`
* Practice `Vagrant`
* Practice `Ansible`

In the end, my main motivation was to practice and learn.

### Requirements
This script is intended for running in `Linux` and create the virtual machines of the laboratory using `libvirt`
* It uses `Vagrant` for deploy the virtual machines
* It uses `Ansible` for automate the installation and configuration of required software

### How to use
The script `k8s-local-cluster.sh` deploys, by default, a `Kubernetes` cluster with **1 controller** and **2 workers**.

This is the exit of `--help` option
```
Local HA Kubernetes cluster deployment tool
Usage:
${0} [options]
Options:
-p,  --project       project name.
                     will override value of env K8SLC_PROJECT
                     default: <current-directory-name>
-m,  --masters       controller node quantity.
                     will override value of env K8SLC_MASTER_QUANTITY
                     default: 1; min: 1; max: 4
-mc, --master-cpus   controller node CPU quantity.
                     will override value of env K8SLC_MASTER_CPUS
                     default: 2; min: 2
-mr, --master-ram    controller node RAM ammount.
                     will override value of env K8SLC_MASTER_RAM
                     default: 2048; min: 2048
-w,  --workers       worker node quantity.
                     will override value of env K8SLC_WORKER_QUANTITY
                     default: 2; min: 1; max: 9
-wc, --worker-cpus   worker node CPU quantity.
                     will override value of env K8SLC_WORKER_CPUS
                     default: 1; min: 1
-wr, --worker-ram    worker node RAM ammount.
                     will override value of env K8SLC_WORKER_RAM
                     default: 1024; min: 1024
-kf, --k8s-flavour   flavour kubernetes-* to install from 
                     https://packages.cloud.google.com/apt/dists.
                     will override value of env K8SLC_K8S_FLAVOUR
                     default: xenial
-kv, --k8s-version   version of packages to install
                     will override value of env K8SLC_K8S_VERSION
                     default: 1.22.3-00
-b,  --box           box of Vagrant to use.
                     will override value of env K8SLC_BOX
                     default: generic/ubuntu1804
-nn, --node-network  IP/24 of the nodes subnet. 
                     will override value of env K8SLC_NODE_NETWORK
                     default: 172.16.100.0
-pn, --pod-network   IP/24 of the nodes subnet. 
                     will override value of env K8SLC_POD_NETWORK
                     default: 172.16.101.0
-h,  --help          prints this help message.
```

### Files that script generates
This script creates the following files in the project directory:
* The file `Vagrantfile`, required by `Vagrant` for the deployment of virtual machines.
* The file `.inventory.ini`, required by `Ansible` for the nodes configuration.
* The directory `.vagrant`, created by `Vagrant` for the management of the virtual machines it has deployed
* The file `.kubeconfig`, required for `kubectl` for the management of the cluster this script has created.


### Management of the created cluster
After a succesfull the deployment, you will be able to manage the cluster using the config file `.kubeconfig`
For instance:
```bash
kubectl --kubeconfig .kubeconfig get nodes
```

### HA topology
If your cluster has more than one control plane, you must add the VIP of the load balancer on your `/etc/hosts` file.
Other wise, `kubectl` will not reach cluster's IP.

After a succesfull deployment, the script will show you the command you should execute to complete this task.

### LoadBalancer
[MetalLB](https://metallb.org/) has been added by default for giving to the cluster load-balancing capacities using standard routing protocols, out the box.
This load-balancer will expose, IPs of node-network (by default: 172.16.100.0/24) from x.x.x.41 to x.x.x.49.

### Dynamic Storage Provisioning
For managing persistent volumes to the cluster, it has been added:
* In infrastructure layer:
  * to main control-plane: a NFS server and client
  * to all nodes: a NFS client 
* To Kubernetes cluster:
  * the [nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner) storage provider

This configuration do not support HA. If main control-plane node goes down, the NFS server, will fall with it.

The simplest way I have find for achive dynamic provisionning is to define a StorageClass and PersistentVolumeClaims, as in this example
```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: test-nfs-sc
provisioner: cluster.local/nfs-subdir-external-provisioner
parameters:
  onDelete: "retain"
  pathPattern: "${.PVC.namespace}/${.PVC.annotations.cluster.local/storage-path}"
mountOptions:
  - lookupcache=pos
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  namespace: default
  name: test-nfs-pvc
  annotations:
    volume.beta.kubernetes.io/storage-class: "test-nfs-sc"
    cluster.local/storage-path: test-nfs-db-data
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 500Mi
```


### Remove your cluster
It is a good practice, to pick up the toys after play with them.
The cluster can be removed with the following command
```bash
Vagrant destroy --force
```
If the configuration of `Vagrant` has been broken, you can also clean your virtualization environment using the command [virsh](https://www.libvirt.org/manpages/virsh.html) or from the user interface that [virt-manager](https://virt-manager.org/) provides.


### Pay attention to known host
When you play a lot with vagrant files, your ~/.ssh/known_hosts can grow very quickly.
Please, keep fit your ~/.ssh/known_hosts file.



