- hosts: master_controller
  vars_files: ['.deploy.yaml']
  tasks:

  - name: Create ClusterConfig file
    block:

    - name: Generate default config
      shell: kubeadm config print init-defaults > ClusterConfiguration.yaml
    
    - name: Configure parameters for pod subnet and load balancer
      vars:
      - additions:
        - regexp: "  serviceSubnet: 10.96.0.0/12"
          line: "  podSubnet: {{ _params.networks.pods }}/24"
        - regexp: "kind: ClusterConfiguration"
          line: "controlPlaneEndpoint: {{ _params.control_pane_endpoint }}"
        - regexp: EOF
          line: |
            ---
            apiVersion: kubelet.config.k8s.io/v1beta1
            kind: KubeletConfiguration
            cgroupDriver: systemd
            clusterDNS:
            - {{  _params.apiserver.vip }}
        - regexp: EOF
          line: |
            ---
            apiVersion: kubeproxy.config.k8s.io/v1alpha1
            kind: KubeProxyConfiguration
            mode: "ipvs"
            ipvs:
              strictARP: true
      lineinfile:
        path: ClusterConfiguration.yaml
        insertafter: "{{ item.regexp }}"
        line: "{{ item.line }}"
        state: present
      with_items: 
      - "{{ additions }}"

    - name: Configure k8s for cotainerd
      vars: 
      - substitutions:
        - regexp: "advertiseAddress"
          line: "  advertiseAddress: {{ _params.apiserver.vip }}"
        - regexp: "criSocket"
          line: "  criSocket: unix:///run/containerd/containerd.sock"
        - regexp: "name"
          line: "  name: {{ _params.haproxy.master }}"
        - regexp: "kubernetesVersion"
          line: "kubernetesVersion: {{ _params.k8s.version }}"
        - regexp: "kubernetesVersion"
          line: "kubernetesVersion: {{ _params.k8s.version }}"
        # - regexp: "  serviceSubnet: 10.96.0.0/12"
        #   line: "  serviceSubnet: 20.96.0.0/12"
      lineinfile:
        path: ClusterConfiguration.yaml
        regexp: "{{ item.regexp }}"
        line: "{{ item.line }}"
        state: present
      with_items: 
      - "{{ substitutions }}"

  - name: Initialize cluster and create join commands
    block:
    - name: Initialize kubeadm
      become: yes
      shell: kubeadm init --config ClusterConfiguration.yaml --upload-certs

    - name: Create join command for controls planes nodes
      block:
      - name: Obtain controller certificate
        become: yes
        # command: kubeadm certs certificate-key
        command: kubeadm init phase upload-certs --upload-certs
        register: controller_certificate

      - name: Create join command with new certificate
        become: yes
        # command: "kubeadm token create --print-join-command --certificate-key {{ controller_certificate.stdout_lines[0] }}"
        command: "kubeadm token create --print-join-command --certificate-key {{ controller_certificate.stdout_lines[2] }}"
        register: controller_join_command

      - name: Save join command in local file
        local_action: copy content="{{ controller_join_command.stdout_lines[0] }}" dest=.master_join_command


    - name: Create join command for worker nodes
      block:
      - name: Create join command
        become: yes
        command: kubeadm token create --print-join-command
        register: worker_join_command

      - name: Save join command in local file
        local_action: copy content="{{ worker_join_command.stdout_lines[0] }}" dest=.worker_join_command


    - name: Controller config files for kubectl
      block:
      - name: create .kube directory
        file:
          path: /home/vagrant/.kube
          state: directory
          mode: 0755

      - name: Check admin.conf file exists.
        stat:
          path: /etc/kubernetes/admin.conf
        register: k8s_conf

      - name: copy admin.conf to user's kube config
        when: k8s_conf.stat.exists
        become: yes
        copy:
          src: /etc/kubernetes/admin.conf
          dest: /home/vagrant/.kube/config
          remote_src: yes
          owner: vagrant


  - name: Create pod network
    block:

    - name: Get default Network policy config file
      get_url: 
        url: https://docs.projectcalico.org/manifests/calico.yaml
        dest: ./calico.yaml
        force: yes

    - name: Configure Network policy
      vars: 
      - substitutions:
        - regexp: "# - name: CALICO_IPV4POOL_CIDR"
          line: "            - name: CALICO_IPV4POOL_CIDR"
        - regexp: "#   value: \"192.168.0.0/16\""
          line: "              value: \"{{ _params.networks.pods }}/24\""
      lineinfile:
        path: calico.yaml
        regexp: "{{ item.regexp }}"
        line: "{{ item.line }}"
        state: present
        backup: yes
      with_items: 
      - "{{ substitutions }}"

    - name: Apply pod network configurarition
      shell: kubectl apply -f calico.yaml

